The CMakeLists.txt requires a couple of variables to be defined in order to use in a stand-alone mode.

The most important is the installation directory, TPL_INSTALL_DIR

In order to direct the installation, use 

-DTPL_INSTALL_DIR="PATH-FOR-INSTALLATION"

Note that to place in system files, this currently requires the target directory
to be setup with write priveleges, e.g., /usr/local/gnu.   Alternatively, if 
you want to provide the lib's and header's system wide, it may be better to run
under su.  
